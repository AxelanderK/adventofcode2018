twochars = 0
threechars = 0

with open('input.txt','r') as fin:
    for line in fin:
        item = line.replace('\n', '')
        chars = list(set(item))
        
        twocounted = False
        threecounted = False

        for char in chars:
            count = item.count(char)
            if count == 2 and not twocounted:
                twochars += 1
                twocounted = True
                #print(char + ': ' + str(count))

                #print(char)
            elif count == 3 and not threecounted:
                threechars += 1
                threecounted = True

print(twochars)
print(threechars)

print(twochars * threechars)

